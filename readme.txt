
*** Synopsis:

This program scales a CT image and converts it to an attenuation mu-map.

Usage: 
./petac <scanner filename> <input filename> <output filename> [ <x-offset> <y-offset> <starting-slice> ]

Example usage:
./petac  scanner.txt AC_CT_BRAIN.mhd MU_MAP_Brain 

The CT can be in several file formats supported by ITK. A CT in DICOM format will always contain a series of individual image files. The easiest way to read a DICOM series is to use the DicomSeriesReadImageWrite program which is supplied with ITK. The DicomSeriesReadImageWrite will convert the CT to a single file so it can be read by petac.

The imaging pipeline is as follows:
 * 1. Apply a gaussian filter which is equal to the crystal size
 * 2. Resample the image to match the dimensions of the PET scanner
 * 3. Apply bilinear scaling
 * 4. Flip or rotate the mu-map if necessary
 * 5. Write an Interfile Header which is understood by STIR

The scanner definition is specified in a text file. The text file containing name=value pairs. For example
number_of_bins=336
number_of_rings=41
bin_size=2
distance_between_rings=4
flip_x=0
flip_y=0
flip_z=1
bin_zoom=1

By default, a mumap is created with the same resolution as the PET detector bin size. The bin_zoom parameter alters the resolution of the mumap.

Keep the same resolution (2mm):
bin_zoom=1

Make the mumap voxel size cover two detector bins (4mm):
bin_zoom=0.5

Sometimes the CT is in a different coordinate system to the scanner geometry. If you find that the mu-map points in the incorrect direction then you may need to apply flipping or a 180 degree rotation. Such transforms can be applied by altering the scanner geometry file. Here are some possible examples of altering the coordinate system using the last three values in the scanner definition file.

No flipping:
flip_x=0
flip_y=0
flip_z=0

Flip along scanner axis:
flip_x=0
flip_y=0
flip_z=1

Rotate 180 degrees:
flip_x=1
flip_y=0
flip_z=1

If the CT covers more than one PET bed position or it is not centred on the  the axis of the PET/CT then you need to use the additional three arguments to the petac program so you mu-map samples the correct bed position. The three arguments are:

x-offset: The x offset of the centre of the CT from the centre of the PET axis (The default is 0mm)
y-offset: The y offset of the centre of the CT from the centre of the PET axis (The default is 0mm)
starting-slice: The slice position of the first slice corresponding to the bed position of the PET scan. (The default is the position of the first slice in the CT series)

*** Installation:

Requirements:

Cmake 2.8 or newer (http://www.cmake.org)
ITK 4.3.2 or newer (http://www.itk.org)

Building (UNIX):

* Run ccmake .
* Set the ITK_DIR to the location of the installed ITK libraries
* Configure and generate project
* Run make

Building (Windows)

* Open cmake and select the petac directory
* Set the ITK_DIR to the location of the installed ITK libraries
* Configure and generate the visual studio project
* Open the generated .sln file in visual studio and build entire project.

This was written by Robbie Barnett rbar9508@uni.sydney.edu.au
