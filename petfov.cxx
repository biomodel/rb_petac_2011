/*
 * This program extracts the PET field of view (FOV) from a mumap image
 * 
 * The imaging pipeline is as follows:
 * 1. Resample the image to match the dimensions of the PET scanner
 * 2. Flip or rotate the mu-map if necessary
 * 3. Write an Interfile Header which is understood by STIR
 * 4. Create a projection image (topogram) to visually inspect the FOV
 *
 * This was written by Robbie Barnett rbar9508@uni.sydney.edu.au
 */

#include "petac.h"


int main( int argc, char* argv[] )
{
	
	if( argc < 4 )
    {
		std::cerr << "Usage: " << std::endl;
		std::cerr << argv[0] << " <scanner filename> <input filename> <output filename> [ <x-offset> <y-offset> <starting-slice> ]" 
		<< std::endl;
		return EXIT_FAILURE;
    }

	if (petac::read_scanner_parameters(argv[1]) > 0) return EXIT_FAILURE;
	
	try {
		ReaderType::Pointer reader = ReaderType::New();
		reader->SetFileName( argv[2] );
		try
		{
			reader->Update();
		}
		catch (itk::ExceptionObject &ex)
		{
			std::cout << ex << std::endl;
			return EXIT_FAILURE;
		}
		typedef itk::ImageFileWriter< ImageType > WriterType;
		WriterType::Pointer writer = WriterType::New();
		
		std::string outputPrefix(argv[3]);
		writer->SetFileName( outputPrefix +   ".mhd");
		
		ImageType::ConstPointer inputImage = reader->GetOutput();
				
		reader -> Update();
		
		const ImageType::SpacingType& inputSpacing = reader->GetOutput()->GetSpacing();
		
		// Determine the region which covers the entire umap
		
		ImageType::RegionType inputRegion = reader->GetOutput()->GetLargestPossibleRegion();	
		ImageType::SizeType inputSize = inputRegion.GetSize(); 
		ImageType::PointType inputOrigin = reader->GetOutput()->GetOrigin();
		
		ImageType::PointType outputOrigin;
		outputOrigin[0] = inputOrigin[0] ;
		outputOrigin[1] = inputOrigin[1];
		outputOrigin[2] = inputOrigin[2] ;
		
		std::cout << "Input origin is " << outputOrigin[0] << "mm, " << outputOrigin[1] << "mm, " << outputOrigin[2] << "mm " << std::endl;
		std::cout << "Input size is " << inputSize[0] << ", " << inputSize[1] << ", " << inputSize[2] << std::endl;
		std::cout << "Input spacing is " << inputSpacing[0] << "mm, " << inputSpacing[1] << "mm, " << inputSpacing[2] << "mm" << std::endl;
		
		
		
		TransformType::Pointer transform = TransformType::New();
		transform->SetIdentity();
		TransformType::ParametersType euler_params = transform -> GetParameters();

		// The initial assumption is that the start of the CT matches the start of the PET.
		// You manually adjust the starting slic corresponding to the appropriate bed position.
		// We also assume that the CT and PET isocentre are the same. A manual shift in the
		// x and y offset can be applied of the x and y offset has been changed
		if( argc >= 7 ) {
			std::cout << "FOV origin would have been: " << outputOrigin[0] << "mm, " << outputOrigin[1] << "mm, " << outputOrigin[2] << "mm " << std::endl;
			euler_params[3] = atof(argv[4]); // Manually shift the xoffset
			euler_params[4] = atof(argv[5]); // Manually shift the yoffset
			outputOrigin[2] = atof(argv[6]);
			std::cout << "FOV origin has been shifted: " << outputOrigin[0] << "mm, " << outputOrigin[1] << "mm, " << outputOrigin[2] << "mm " << std::endl;
			if (argc >= 10) {
			  euler_params[0] = atof(argv[7]); // Rotation about X axis
			  euler_params[1] = atof(argv[8]);  // Rotation about Y axis
			  euler_params[2] = atof(argv[9]);  // Rotation about Z axis
			  std::cout << "FOV is rotation is: " << euler_params[0] << "rad, "  << euler_params[1] << "rad, "  << euler_params[2] << "rad" << std::endl;
			}
		}
		
		// Create a filter to resample the umap
		
		typedef itk::ResampleImageFilter<
		ImageType, ImageType >  ResampleFilterType;
		ResampleFilterType::Pointer resampler = ResampleFilterType::New();
		
		transform->SetParameters(euler_params);
		
		resampler->SetDefaultPixelValue( 0 ); // Default pixel value to air ( 0 cm-1 )
		

		if (petac::set_resampler_output(resampler, transform, inputOrigin, inputSize, inputSpacing, outputOrigin) > 0) return EXIT_FAILURE;
 

		resampler->SetInput( inputImage   );
		
		// Flip about some axis so that the PET and CT point in the same direction
		// 180 degree rotation may be necessary for wholebody scans where patient is lying with head pointing our of scanner.
		
		FlipType::Pointer flipFilter = FlipType::New();

		flipFilter -> SetFlipAxes(petac::get_axes());
 	
		flipFilter -> SetInput(resampler->GetOutput());	
		writer->SetInput( flipFilter->GetOutput() );
		
		ImageType::PointType writeOrigin = flipFilter->GetOutput()->GetOrigin();
		
		
		std::cout  << "Writing the FOV to file " << outputPrefix << ".mhd" << std::endl;
		try
		{
			writer->Update();
		}
		catch (itk::ExceptionObject &ex)
		{
			std::cout << ex << std::endl;
			return EXIT_FAILURE;
		}

		petac::create_topogram(inputImage, outputPrefix + "_entire.png");
		petac::create_topogram(resampler->GetOutput(), outputPrefix + "_fov.png");

	}
	
	
	
	catch (itk::ExceptionObject &ex)
    {
		std::cout << ex << std::endl;
		return EXIT_FAILURE;
    }
	
	return EXIT_SUCCESS;
	
}
