
#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"
#include "itkAddImageFilter.h"
#include "itkMultiplyImageFilter.h"
#include "itkSubtractImageFilter.h"
#include "itkShiftScaleImageFilter.h"
#include "itkThresholdImageFilter.h"
#include "itkExtractImageFilter.h"
#include "itkResampleImageFilter.h"
#include "itkShrinkImageFilter.h"
#include "itkLinearInterpolateImageFunction.h" 
#include "itkCenteredAffineTransform.h" 
#include "itkFlipImageFilter.h"
#include "itkIdentityTransform.h"
#include "itkRecursiveGaussianImageFilter.h"
#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <itkAccumulateImageFilter.h>
#include <itkRescaleIntensityImageFilter.h>
#include <itkEuler3DTransform.h>
#include <itkRegionOfInterestImageFilter.h>
#include <itkMaskImageFilter.h>
#include <itkMaskNegatedImageFilter.h>
#include "itkTileImageFilter.h"
#include <itkAccumulateImageFilter.h>
#include <itkStatisticsImageFilter.h>


#ifndef PETAC_H
#define PETAC_H


#define PI (3.141592653589793)


typedef std::multimap<std::string, std::string> StringMapType;
typedef std::pair<std::string, std::string> StringPairType;

typedef float    PixelType;
const unsigned int      Dimension = 3;
const unsigned int      TimeDim = 4;
typedef itk::Image< PixelType, Dimension >         ImageType;
typedef itk::Image< PixelType, TimeDim >         Image4DType;
typedef itk::TileImageFilter< ImageType, Image4DType > TilerType;
typedef itk::ImageFileReader< ImageType >        ReaderType;
typedef itk::ShiftScaleImageFilter< ImageType, ImageType > ShiftScaleFilterType; 
typedef itk::AddImageFilter<ImageType, ImageType, ImageType > AddFilterType; 
typedef itk::MultiplyImageFilter<ImageType, ImageType, ImageType > MultiplyFilterType; 
typedef itk::SubtractImageFilter<ImageType, ImageType, ImageType > SubtractFilterType; 
typedef itk::ThresholdImageFilter< ImageType > ThresholdFilterType; 
typedef itk::ExtractImageFilter< ImageType, ImageType > ExtractFilterType; 
typedef itk::FlipImageFilter< ImageType > FlipType;
typedef itk::RegionOfInterestImageFilter<ImageType, ImageType> RegionFilter;
   
// PET/CT: Comparison of Quantitative Tracer Uptake Between Germanium and CT Transmission Attenuation-Corrected Images 
// http://jnm.snmjournals.org/cgi/content/figsonly/43/9/1137
const float bilinear_scale_hu[] = {-1000,0,3071};
const float bilinear_scale_mu[] = {0,0.093,0.262};

// Compute the y intercept of each of the bilinear scales
// y = m * (x + h )
// mu = m * ( hu + h )
// h =  mu / m - hu

// Compute constants for the ShiftScaleImageFilter

const float m1 = (bilinear_scale_mu[1]-bilinear_scale_mu[0])/(bilinear_scale_hu[1]-bilinear_scale_hu[0]); // The slope
const float shiftScale1[] = {
  bilinear_scale_mu[1]/m1-bilinear_scale_hu[1], // The y intercept	
  m1
};
const float m2 = (bilinear_scale_mu[2]-bilinear_scale_mu[1])/(bilinear_scale_hu[2]-bilinear_scale_hu[1]); // The slope
const float shiftScale2[] = {
  bilinear_scale_mu[1]/m2-bilinear_scale_hu[1], // The y intercept
  m2
};

const float ADD_FILTER_DELTA = 1e-8; // This is used when adding bone and tissue images together


typedef itk::ResampleImageFilter< ImageType, ImageType >  ResampleFilterType;
typedef itk::ImageFileWriter< ImageType > WriterType;

typedef itk::AccumulateImageFilter< ImageType, ImageType > AccumulateFilterType;
typedef unsigned char BitmapPixelType;
typedef itk::Image< BitmapPixelType, 3 >         BitmapVolumeType;
typedef itk::Image< BitmapPixelType, 2 >         BitmapType;
typedef itk::RescaleIntensityImageFilter< ImageType, BitmapVolumeType> RescaleFilterType;
typedef itk::ImageFileWriter< BitmapType > BitmapWriterType;
typedef itk::ExtractImageFilter< BitmapVolumeType, BitmapType > ExtractBitmapType; 
typedef itk::Euler3DTransform< double>  TransformType;
typedef itk::MaskImageFilter< ImageType, ImageType, ImageType> MaskFilterType;
typedef itk::MaskNegatedImageFilter< ImageType, ImageType, ImageType> MaskNegatedFilterType;

namespace petac {
  int check_output_region(ImageType::PointType inputOrigin, ImageType::SizeType inputSize, ImageType::SpacingType inputSpacing, ImageType::PointType outputOrigin, ImageType::SizeType outputSize, ImageType::SpacingType outputSpacing);
  int set_resampler_output(ResampleFilterType::Pointer resampler, TransformType::Pointer transform, ImageType::PointType inputOrigin, ImageType::SizeType inputSize, ImageType::SpacingType inputSpacing, ImageType::PointType outputOrigin);
  int read_scanner_parameters(char* parameter_filename);
  int write_if_header(std::string outputPrefix, ResampleFilterType::Pointer resampler, WriterType::Pointer writer, ImageType::PointType writeOrigin);
  float get_bin_size();
  int create_topogram(ImageType::ConstPointer imageVolume, std::string outputFilename);
  FlipType::FlipAxesArrayType get_axes();
}

#endif /* PETAC_H */
