
#include "petac.h"

int shrinkTile(ImageType::Pointer inputImage, TilerType::Pointer tiler, unsigned int inputImageNumber,  unsigned int shrinkFactor[3]) {
   
  typedef itk::ShrinkImageFilter <ImageType, ImageType>
          ShrinkImageFilterType;
 
  ShrinkImageFilterType::Pointer shrinkFilter
          = ShrinkImageFilterType::New();	

//  inputImage->DisconnectPipeline();
  shrinkFilter->SetInput(inputImage);

  shrinkFilter->SetShrinkFactor(0, shrinkFactor[0]);
  shrinkFilter->SetShrinkFactor(1, shrinkFactor[1]);
  shrinkFilter->SetShrinkFactor(2, shrinkFactor[2]);

  shrinkFilter->Update();
  ImageType::Pointer smallImage;
  smallImage = shrinkFilter->GetOutput();
   
 std::cout << "New size: " << smallImage->GetLargestPossibleRegion().GetSize() << std::endl;
   
    tiler->SetInput( inputImageNumber, smallImage );
}

int main(int argc, char *argv[] )
{
 
 
  typedef itk::ImageFileReader< ImageType > ImageReaderType;
 
  typedef itk::ImageFileWriter< Image4DType > WriterType;
 
  if (argc < 7)
    {
    std::cerr << "Usage: " << std::endl;
    std::cerr << argv[0] << "shrinkX shrinkY shrinkZ input1 input2 ... inputn output outputMask" << std::endl;
    return EXIT_FAILURE;
    }
 
  TilerType::Pointer tiler = TilerType::New();
 
  itk::FixedArray< unsigned int, TimeDim > layout;
 
  layout[0] = 1;
  layout[1] = 1;
  layout[2] = 1;
  layout[3] = 0;
 
  tiler->SetLayout( layout );
 
  unsigned int inputImageNumber = 0;
 
  ImageReaderType::Pointer reader = ImageReaderType::New();
 
  ImageType::Pointer inputImageTile;
 
  unsigned int shrinkFactor[3];
  shrinkFactor[0] = atoi(argv[1]);
  shrinkFactor[1] = atoi(argv[2]);
  shrinkFactor[2] = atoi(argv[3]);
  
  assert(shrinkFactor[0] >= 1);
  assert(shrinkFactor[1] >= 1);
  assert(shrinkFactor[2] >= 1);

  for (int i = 4; i < argc - 2; i++)
    {
    reader->SetFileName( argv[i] );
    reader->UpdateLargestPossibleRegion();
    inputImageTile = reader->GetOutput();
    shrinkTile(inputImageTile,tiler,inputImageNumber++, shrinkFactor);
//    inputImageTile->DisconnectPipeline();
//    tiler->SetInput( inputImageNumber++, inputImageTile );
    }
 
  PixelType filler = 0;
 
  tiler->SetDefaultPixelValue( filler );
 
  tiler->Update();
 
  WriterType::Pointer writer = WriterType::New();
  writer->SetInput( tiler->GetOutput() );
  writer->SetFileName( argv[argc-2] );
 
  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & excp )
    {
    std::cerr << excp << std::endl;
    return EXIT_FAILURE;
    }


  typedef itk::StatisticsImageFilter<Image4DType> StatisticsImageFilterType;
  StatisticsImageFilterType::Pointer statisticsImageFilter
          = StatisticsImageFilterType::New ();
  statisticsImageFilter->SetInput(tiler->GetOutput());
  statisticsImageFilter->Update();

   std::cout << "Std.: " << statisticsImageFilter->GetSigma() << std::endl;

   typedef itk::AccumulateImageFilter< Image4DType, Image4DType > AccumulateFilterType;
   AccumulateFilterType::Pointer accumulateFilter = AccumulateFilterType::New();
   accumulateFilter->SetInput( tiler->GetOutput() );
   accumulateFilter->SetAccumulateDimension( 3 );
   accumulateFilter->SetAverage (true);
   
    accumulateFilter -> Update();
   typedef itk::ExtractImageFilter< Image4DType, ImageType > ExtractFilterType;
   ExtractFilterType::Pointer extractFilter = ExtractFilterType::New();
   extractFilter -> SetInput(accumulateFilter -> GetOutput());
   extractFilter -> SetDirectionCollapseToSubmatrix(); // Use this when programmer knows 4D image is 3D+time
   Image4DType::RegionType region4D;
   region4D = accumulateFilter -> GetOutput() -> GetLargestPossibleRegion();
   region4D.SetSize(3,0);
   extractFilter -> SetExtractionRegion(region4D);
      

   typedef itk::ThresholdImageFilter< ImageType > ThresholdFilterType;
   ThresholdFilterType::Pointer thresholdFilter = ThresholdFilterType::New();
   thresholdFilter -> SetInput(extractFilter->GetOutput() );
   thresholdFilter -> ThresholdBelow(statisticsImageFilter->GetSigma());
   inputImageTile = thresholdFilter -> GetOutput();

   tiler = TilerType::New();
   tiler->SetLayout( layout );
   inputImageNumber = 0;
   
    for (int i = 4; i < argc - 2; i++)
    {
       tiler->SetInput( inputImageNumber++, inputImageTile );
    }

  tiler->SetDefaultPixelValue( filler );

  tiler->Update();


 
  writer = WriterType::New();
  writer->SetInput( tiler->GetOutput() );
  writer->SetFileName( argv[argc-1] );

   try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & excp )
    {
    std::cerr << excp << std::endl;
    return EXIT_FAILURE;
    }


   
  return EXIT_SUCCESS;
}
