
#include "petac.h"

namespace petac {
  int number_of_bins = 0; int number_of_rings = 0; 
  float bin_size = 0; float distance_between_rings = 0; float bin_zoom=1;
  int flip_x = 0; int flip_y=0; int flip_z=0;
  float z_margin=0;
}
		
int petac::check_output_region(ImageType::PointType inputOrigin, ImageType::SizeType inputSize, ImageType::SpacingType inputSpacing, ImageType::PointType outputOrigin, ImageType::SizeType outputSize, ImageType::SpacingType outputSpacing) {
  int ret = 0;
  if (outputOrigin[0] > inputOrigin[0] + inputSpacing[0]*inputSize[0]) {
    std::cerr << "Error: Output region is off the edge of the input image (X output origin too big)" << std::endl;
    ret = 2;
  }
  if (outputOrigin[1] > inputOrigin[1] + inputSpacing[1]*inputSize[1]) {
    std::cerr << "Error: Output region is off the edge of the input image (Y output origin too big)" << std::endl;
    ret = 2;
  }
  if (outputOrigin[2] > inputOrigin[2] + inputSpacing[2]*inputSize[2]) {
    std::cerr << "Error: Output region is off the edge of the input image (Z output origin too big)" << std::endl;
    ret = 2;
  }
  if (outputOrigin[0]+outputSpacing[0]*outputSize[0] < inputOrigin[0]) {
    std::cerr << "Error: Output region is off the edge of the input image (X output endpoint too small)" << std::endl;
    ret = 2;
  }
  if (outputOrigin[1]+outputSpacing[1]*outputSize[1] < inputOrigin[1]) {
    std::cerr << "Error: Output region is off the edge of the input image (Y output endpoint too small)" << std::endl;
    ret = 2;
  }
  if (outputOrigin[2]+outputSpacing[2]*outputSize[2] < inputOrigin[2]) {
    std::cerr << "Error: Output region is off the edge of the input image (Z output endpoint too small)" << std::endl;
    ret = 2;
  }
  if (ret > 0) {
    std::cerr << "Input Origin = (" << inputOrigin[0] << ", " << inputOrigin[1] << ", " << inputOrigin[2] << ")" << std::endl;
    std::cerr << "Input Size = (" << inputSize[0] << ", " << inputSize[1] << ", " << inputSize[2] << ")" << std::endl;
    std::cerr << "Input Spacing = (" << inputSpacing[0] << ", " << inputSpacing[1] << ", " << inputSpacing[2] << ")" << std::endl;
 
    std::cerr << "Output Origin = (" << outputOrigin[0] << ", " << outputOrigin[1] << ", " << outputOrigin[2] << ")" << std::endl;
    std::cerr << "Output Size = (" << outputSize[0] << ", " << outputSize[1] << ", " << outputSize[2] << ")" << std::endl;
    std::cerr << "Output Spacing = (" << outputSpacing[0] << ", " << outputSpacing[1] << ", " << outputSpacing[2] << ")" << std::endl;
  
  }

  return ret;
}

int petac::set_resampler_output(ResampleFilterType::Pointer resampler, TransformType::Pointer transform, ImageType::PointType inputOrigin, ImageType::SizeType inputSize, ImageType::SpacingType inputSpacing, ImageType::PointType outputOrigin) {

  resampler->SetTransform( transform );

    typedef ImageType::SizeType::SizeValueType SizeValueType;
    ImageType::SpacingType outputSpacing;
    ImageType::SizeType   outputSize;
    
    // Set the resolution equal to the crystal size
    
    float new_bin_size = petac::bin_size/petac::bin_zoom;
    outputSpacing[0] = new_bin_size;
    outputSpacing[1] = new_bin_size;
    outputSpacing[2] = distance_between_rings/2;
    
    /// The number of slices in the z margin
    SizeValueType dz_margin = static_cast<SizeValueType>(petac::z_margin/outputSpacing[2]);

    // Account for the fact that the isocentores of the input and output pixels could be difference
    //  outputOrigin[0] += outputSpacing[0]/2.0-inputSpacing[0]/2.0;
    //    outputOrigin[1] += outputSpacing[1]/2.0-inputSpacing[1]/2.0;
	
    resampler->SetOutputSpacing( outputSpacing );
    
    if (petac::check_output_region(inputOrigin, inputSize, inputSpacing, outputOrigin, outputSize, outputSpacing) > 0) return 2;
    
    // Set the size of the MU map to cover the same volume as the CT
    
    double dx = inputSize[0] * inputSpacing[0] / outputSpacing[0];
    double dy = inputSize[1] * inputSpacing[1] / outputSpacing[1];
    double dz = 2*petac::number_of_rings-1;
    
    int pet_fov = petac::number_of_bins*petac::bin_zoom+1;
    
    // Expand the size of the MU map to over the entire field of view
    if ((dx+1)*outputSpacing[0] < pet_fov*new_bin_size) {
      std::cout << "MuMap X size too small: " << dx << std::endl;
      outputOrigin[0] += (dx*outputSpacing[0] - pet_fov*new_bin_size)/2.0;
      dx = pet_fov*new_bin_size/outputSpacing[0];
      std::cout << "MuMap X size expanded to: " << dx << std::endl;
    }
    if ((dy+1)*outputSpacing[1] < pet_fov*new_bin_size) {
      std::cout << "MuMap Y size too small: " << dy << std::endl;
      outputOrigin[1] += (dy*outputSpacing[1] - pet_fov*new_bin_size)/2.0;
      dy = pet_fov*new_bin_size/outputSpacing[1];
      std::cout << "MuMap Y size expanded to: " << dy << std::endl;
    }

    if (dz_margin > 0) {
	std::cout << "Expaning the number of axial slices by " << dz_margin << " at each end of the image." << std::endl;
	outputOrigin[2] -= dz_margin*outputSpacing[2];
	dz +=  2*dz_margin;
//	TransformType::ParametersType euler_params = transform -> GetParameters();
//   	euler_params[5] -= dz_margin*outputSpacing[2]; 
//	transform -> SetParameters(euler_params);
   }

    resampler->SetOutputOrigin( outputOrigin );
    
    outputSize[0] = static_cast<SizeValueType>( dx );
    outputSize[1] = static_cast<SizeValueType>( dy );
    outputSize[2] = static_cast<SizeValueType>( dz );
  
    TransformType::ParametersType  outputCenter = transform-> GetFixedParameters(); 
    for (int d=0; d < 3; d++) {
      outputCenter[d] = outputOrigin[d] + outputSpacing[d]*(outputSize[d]-1)/2.0;
	}
  
    std::cout << "MuMap origin is " << outputOrigin[0] << "mm, " << outputOrigin[1] << "mm, " << outputOrigin[2] << "mm " << std::endl;
    std::cout << "MuMap size is " << outputSize[0] << ", " << outputSize[1] << ", " << outputSize[2] << std::endl;
    std::cout << "MuMap spacing: " << outputSpacing[0] << "mm, " << outputSpacing[1] << "mm, " << outputSpacing[2] << "mm " << std::endl;
    std::cout << "MuMap center: " << outputCenter[0] << "mm, " << outputCenter[1] << "mm, " << outputCenter[2] << "mm " << std::endl;
    
    transform->SetFixedParameters(outputCenter);
    resampler->SetSize(outputSize );

  
    return 0;
}
   
int petac::read_scanner_parameters(char* parameter_filename) {
	
	// Scanner definition	
	std::ifstream scannerFile;
	scannerFile.open (parameter_filename);
	if (! scannerFile.good()) {
		std::cerr << "Scanner parameter file not found: " << parameter_filename << std::endl;
		return EXIT_FAILURE;
	}

	StringMapType scannerParameters;
	std::string line;
	while (scannerFile.good()) {
	  scannerFile >> line;
	  int lpos=line.find('=');
	  if (lpos >= 0) {
	    StringPairType pair(line.substr(0,lpos),line.substr(lpos+1));
	    scannerParameters.insert( pair);
	  }
	}
	scannerFile.close();
	StringMapType::iterator it;
	it = scannerParameters.find("number_of_bins");
        if (it != scannerParameters.end()) petac::number_of_bins = atoi((it->second).c_str());
	it = scannerParameters.find("number_of_rings");
        if (it != scannerParameters.end()) petac::number_of_rings = atoi((it->second).c_str());
	it = scannerParameters.find("bin_size");
        if (it != scannerParameters.end())  petac::bin_size = atof((it->second).c_str());
	it = scannerParameters.find("bin_zoom");
        if (it != scannerParameters.end())  petac::bin_zoom = atof((it->second).c_str());
	it = scannerParameters.find("distance_between_rings");
        if (it != scannerParameters.end()) petac::distance_between_rings = atof((it->second).c_str());
	it = scannerParameters.find("flip_x");
        if (it != scannerParameters.end()) petac::flip_x = atoi((it->second).c_str());
	it = scannerParameters.find("flip_y");
        if (it != scannerParameters.end()) petac::flip_y = atoi((it->second).c_str());
	it = scannerParameters.find("flip_z");
        if (it != scannerParameters.end()) petac::flip_z = atoi((it->second).c_str());
	it = scannerParameters.find("z_margin");
        if (it != scannerParameters.end()) petac::z_margin = atof((it->second).c_str());
	
	std::cout << "Scanner definition:" << std::endl;
	std::cout << "Number of bins: " << petac::number_of_bins << ", Number of rings: " << petac::number_of_rings << std::endl;
	std::cout << "Bin size: " << petac::bin_size << "mm, Distance between rings: " << petac::distance_between_rings << "mm, Bin zoom: " << petac::bin_zoom << std::endl;
	std::cout << "Flip axis: " << petac::flip_x << ", " << petac::flip_y << ", "  << petac::flip_z << std::endl;
	std::cout << "Z margin: " << petac::z_margin << std::endl;

	if ((petac::number_of_bins < 1) or (petac::number_of_rings < 1) or (petac::bin_size < ADD_FILTER_DELTA) or (petac::distance_between_rings < ADD_FILTER_DELTA) or (petac::bin_zoom < ADD_FILTER_DELTA)) {
		std::cerr << "Scanner definition invalid: " << parameter_filename << std::endl;
		return EXIT_FAILURE;
	}
	
	return 0;
}

int petac::write_if_header(std::string outputPrefix, ResampleFilterType::Pointer resampler, WriterType::Pointer writer, ImageType::PointType writeOrigin) {
  
  // Write interfile header in the PET scanner coordinate system
  std::ofstream   iout;
  iout.open( (outputPrefix + ".hv").c_str() );
  if( iout.fail() )
    {
      std::cerr << "Error writing to Interfile." << std::endl;
      return 2;
    }
  
  ImageType::SpacingType outputSpacing = resampler -> GetOutputSpacing();
  ImageType::SizeType outputSize = resampler -> GetSize();
  // The MU map is in the PET scanner coordinate system where x=0 and y=0 is at the isocentre.
  // z=0 is the position of the first ring
  writeOrigin[0] = -outputSpacing[0]*(outputSize[0]-1)/2.0;
  writeOrigin[1] = -outputSpacing[1]*(outputSize[1]-1)/2.0;
  writeOrigin[2] = 0;
  
  ImageType::RegionType writeRegion = writer->GetInput()->GetLargestPossibleRegion();	
  ImageType::SizeType writeSize = writeRegion.GetSize(); 
  const ImageType::SpacingType& writeSpacing = writer->GetInput()->GetSpacing();
  
  iout << "!INTERFILE := " << std::endl;
  iout << "name of data file := " << outputPrefix << ".raw " << std::endl;
  iout << "!GENERAL DATA :=  " << std::endl;
  iout << "!GENERAL IMAGE DATA := " << std::endl; 
  iout << "!type of data := PET " << std::endl;
  iout << "imagedata byte order := LITTLEENDIAN " << std::endl;
  iout << "!PET STUDY (General) :=  " << std::endl;
  iout << "!PET data type := Image " << std::endl;
  iout << "process status := Reconstructed " << std::endl;
  iout << "!number format := float " << std::endl;
  iout << "!number of bytes per pixel := 4 " << std::endl;
  iout << "number of dimensions := 3 " << std::endl;
  iout << "matrix axis label [1] := x " << std::endl;
  iout << "!matrix size [1] := " << writeSize[0] << std::endl;
  iout << "scaling factor (mm/pixel) [1] := " << writeSpacing[0] << std::endl;
  iout << "matrix axis label [2] := y " << std::endl;
  iout << "!matrix size [2] := " << writeSize[1] << std::endl;
  iout << "scaling factor (mm/pixel) [2] := " << writeSpacing[1] << std::endl;
  iout << "matrix axis label [3] := z " << std::endl;
  iout << "!matrix size [3] := " << writeSize[2] << std::endl;
  iout << "scaling factor (mm/pixel) [3] := " << writeSpacing[2] << std::endl;
  iout << "first pixel offset (mm) [1] := " << writeOrigin[0] << std::endl;
  iout << "first pixel offset (mm) [2] := " << writeOrigin[1] << std::endl;
  iout << "first pixel offset (mm) [3] := " << writeOrigin[2] << std::endl;
  iout << "number of time frames := 1 " << std::endl;
  iout << "image scaling factor[1] := 1 " << std::endl;
  iout << "data offset in bytes[1] := 0 " << std::endl;
  iout << "quantification units := 1 " << std::endl;
  return 0;
  
}

float petac::get_bin_size() {
  return petac::bin_size;
}

FlipType::FlipAxesArrayType petac::get_axes() {
  FlipType::FlipAxesArrayType axes;
  axes[0] = petac::flip_x > 0;
  axes[1] = petac::flip_y > 0;
  axes[2] = petac::flip_z > 0;
  return axes;
}

int petac::create_topogram(ImageType::ConstPointer imageVolume, std::string outputFilename) {


  AccumulateFilterType::Pointer accumulateFilter = AccumulateFilterType::New();
  accumulateFilter -> SetInput(imageVolume);
  accumulateFilter -> SetAccumulateDimension(1);

  RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
  rescaleFilter->SetInput(accumulateFilter->GetOutput());

  accumulateFilter->Update();

  ImageType::RegionType inputRegion = accumulateFilter->GetOutput()->GetLargestPossibleRegion();
 
  // std::cout << "Input region size is: "  << inputRegion.GetSize() << std::endl;
  ImageType::SizeType desiredSize =  inputRegion.GetSize();
  desiredSize[1] = 0;
  ImageType::IndexType desiredStart = inputRegion.GetIndex();
  ImageType::RegionType desiredRegion(desiredStart, desiredSize);
  //  std::cout << "Desired region size is: "  << desiredSize << std::endl;

  ExtractBitmapType::Pointer extractFilter = ExtractBitmapType::New();
  extractFilter->SetInput(rescaleFilter->GetOutput());
  extractFilter->SetExtractionRegion(desiredRegion);
  extractFilter->SetDirectionCollapseToIdentity();

  BitmapWriterType::Pointer writer = BitmapWriterType::New();
  writer->SetInput(extractFilter->GetOutput());
  writer->SetFileName(outputFilename);
  try
    {
      writer->Update();
    }
  catch (itk::ExceptionObject &ex)
    {
      std::cout << ex << std::endl;
      return EXIT_FAILURE;
    }	
  return 0;
}
