/*
 * This program scales a CT image and converts it to an attenuation MUMAP.
 * 
 * The imaging pipeline is as follows:
 * 1. Apply a gaussian filter which is equal to the crystal size
 * 2. Resample the image to match the dimensions of the PET scanner
 * 3. Apply bilinear scaling
 * 4. Flip or rotate the mu-map if necessary
 * 5. Write an Interfile Header which is understood by STIR
 *
 * This was written by Robbie Barnett rbar9508@uni.sydney.edu.au
 */

#include "petac.h"

int main( int argc, char* argv[] )
{
	
  if( argc < 4 )
    {
      std::cerr << "Usage: " << std::endl;
      std::cerr << argv[0] << " <scanner filename> <input filename> <output filename> [ <x-offset> <y-offset> <starting-slice> ]" 
		<< std::endl;
      return EXIT_FAILURE;
    }
  
  if (petac::read_scanner_parameters(argv[1]) > 0) return EXIT_FAILURE;
  
  try {
    ReaderType::Pointer reader = ReaderType::New();
    reader->SetFileName( argv[2] );
    try
      {
	reader->Update();
      }
    catch (itk::ExceptionObject &ex)
      {
	std::cout << ex << std::endl;
	return EXIT_FAILURE;
      }
    WriterType::Pointer writer = WriterType::New();
    
    std::string outputPrefix(argv[3]);
    writer->SetFileName( outputPrefix +   ".mhd");
    
    // Create the gaussian filters in the X and Y directions
    
    typedef itk::RecursiveGaussianImageFilter< 
    ImageType,
      ImageType > GaussianFilterType;
    
    GaussianFilterType::Pointer smootherX = GaussianFilterType::New();
    GaussianFilterType::Pointer smootherY = GaussianFilterType::New();
    
    smootherX->SetInput( reader->GetOutput() );
    smootherY->SetInput( smootherX->GetOutput() );
    
    ImageType::ConstPointer inputImage = reader->GetOutput();
    
    
    smootherX->SetSigma( petac::get_bin_size() );
    smootherY->SetSigma( petac::get_bin_size() );
    
    smootherX->SetDirection( 0 );
    smootherY->SetDirection( 1 );
    
    smootherX->SetOrder( GaussianFilterType::ZeroOrder );
    smootherY->SetOrder( GaussianFilterType::ZeroOrder );
    
    // https://mri.radiology.uiowa.edu/mediawiki/index.php/Gaussian_Filter_Example
    // SetNormalizeAcrossScale- Set/Get the flag for normalizing the gaussian over scale space.
    // When this flag is ON the filter will be normalized in such a way
    // that larger sigmas will not result in the image fading away.
    
    smootherX->SetNormalizeAcrossScale( false );
    smootherY->SetNormalizeAcrossScale( false );
    
    
    reader -> Update();
    
    const ImageType::SpacingType& inputSpacing = reader->GetOutput()->GetSpacing();
    
    // Determine the region which covers the entire CT
    
    ImageType::RegionType inputRegion = reader->GetOutput()->GetLargestPossibleRegion();	
    ImageType::SizeType inputSize = inputRegion.GetSize(); 
    ImageType::PointType inputOrigin = reader->GetOutput()->GetOrigin();
    
    ImageType::PointType outputOrigin;
    outputOrigin[0] = inputOrigin[0];
    outputOrigin[1] = inputOrigin[1];
    outputOrigin[2] = inputOrigin[2];
    
    std::cout << "CT origin is " << outputOrigin[0] << "mm, " << outputOrigin[1] << "mm, " << outputOrigin[2] << "mm " << std::endl;
    std::cout << "CT size is " << inputSize[0] << ", " << inputSize[1] << ", " << inputSize[2] << std::endl;
    std::cout << "CT spacing is " << inputSpacing[0] << "mm, " << inputSpacing[1] << "mm, " << inputSpacing[2] << "mm" << std::endl;
    
    // The initial assumption is that the start of the CT matches the start of the PET.
    // You manually adjust the starting slic corresponding to the appropriate bed position.
    // We also assume that the CT and PET isocentre are the same. A manual shift in the
    // x and y offset can be applied of the x and y offset has been changed
    if( argc >= 7 ) {
      std::cout << "MuMap origin would have been: " << outputOrigin[0] << "mm, " << outputOrigin[1] << "mm, " << outputOrigin[2] << "mm " << std::endl;
      outputOrigin[0] += atof(argv[4]); // Manually shift the xoffset
      outputOrigin[1] += atof(argv[5]); // Manually shift the yoffset
      outputOrigin[2] = atof(argv[6]);
      std::cout << "MuMap origin has been shifted. " << std::endl;
    }
    
    // Create a filter to resample the CT
    
   
    ResampleFilterType::Pointer resampler = ResampleFilterType::New();
    
    TransformType::Pointer transform = TransformType::New();
    transform->SetIdentity();
    //   resampler->SetTransform( transform );
    
    typedef itk::LinearInterpolateImageFunction< 
    ImageType, double >  InterpolatorType;
    
    InterpolatorType::Pointer interpolator = InterpolatorType::New();
    
    resampler->SetInterpolator( interpolator );
    
    resampler->SetDefaultPixelValue( -1000 ); // Default pixel value to air ( -1000HU )
    
    if (petac::set_resampler_output(resampler, transform, inputOrigin, inputSize, inputSpacing, outputOrigin) > 0) return EXIT_FAILURE;
 
 
    resampler->SetInput( smootherY->GetOutput()  );
    
    // Apply bilienar scale to convert hounsfield units to linear attenuation coefficients
    
    // Create two scaled images. The first image scales tissue, the second scales bone.
    ShiftScaleFilterType::Pointer shiftFilter1 = ShiftScaleFilterType::New(); 
    ShiftScaleFilterType::Pointer shiftFilter2 = ShiftScaleFilterType::New(); 
    shiftFilter1->SetInput(resampler->GetOutput());
    shiftFilter2->SetInput(resampler->GetOutput());
    
    // Tissue equivalent scaling
    shiftFilter1->SetShift(shiftScale1[0]);
    shiftFilter1->SetScale(shiftScale1[1]);
    
    // Bone equivalent scaling
    shiftFilter2->SetShift(shiftScale2[0]);
    shiftFilter2->SetScale(shiftScale2[1]);
    
    // Prepare images for adding back together by applying a threshold
    ThresholdFilterType::Pointer thresholdFilter1 = ThresholdFilterType::New(); 
    ThresholdFilterType::Pointer thresholdFilter2 = ThresholdFilterType::New(); 
    ThresholdFilterType::Pointer thresholdFilter2u = ThresholdFilterType::New(); 
    thresholdFilter1->SetInput( shiftFilter1->GetOutput() ); 
    thresholdFilter2->SetInput( shiftFilter2->GetOutput() ); 
    thresholdFilter2u->SetInput( thresholdFilter2->GetOutput() ); 
    
    // Threshold so that we see tissue only, everything else is zero
    thresholdFilter1->SetOutsideValue( 0 ); 
    thresholdFilter1->ThresholdAbove( bilinear_scale_mu[1] ); 
    
    // Threshold so that we see bone only, everything else is zero
    thresholdFilter2->SetOutsideValue( 0 ); 
    thresholdFilter2->ThresholdBelow( bilinear_scale_mu[1] + ADD_FILTER_DELTA); 
    
    // Do not set values greater than the maximum possible mu value
    thresholdFilter2u->SetOutsideValue( bilinear_scale_mu[2] ); 
    thresholdFilter2u->ThresholdAbove( bilinear_scale_mu[2] ); 
    
    // Add images back together
    AddFilterType::Pointer addFilter = AddFilterType::New(); 
    addFilter->SetInput1( thresholdFilter1->GetOutput() ); 
    addFilter->SetInput2( thresholdFilter2u->GetOutput() ); 
    
    // Flip about some axis so that the PET and CT point in the same direction
    // 180 degree rotation may be necessary for wholebody scans where patient is lying with head pointing our of scanner.
    
    FlipType::Pointer flipFilter = FlipType::New();
 
    flipFilter -> SetFlipAxes(petac::get_axes());
    
    flipFilter -> SetInput(addFilter->GetOutput());	
    writer->SetInput( flipFilter->GetOutput() );
    
    ImageType::PointType writeOrigin = flipFilter->GetOutput()->GetOrigin();
    
    
    std::cout  << "Writing the MuMap to file " << outputPrefix << ".mhd" << std::endl;
    try
      {
	writer->Update();
      }
    catch (itk::ExceptionObject &ex)
      {
	std::cout << ex << std::endl;
	return EXIT_FAILURE;
      }
    
    std::cout  << "Writing the MuMap to interfile (scanner coordinates) " << outputPrefix << ".hv" << std::endl;
    
    if ( petac::write_if_header(outputPrefix, resampler, writer, writeOrigin ) > 0) return EXIT_FAILURE;
    
    
    
  } catch (itk::ExceptionObject &ex)
    {
      std::cout << ex << std::endl;
      return EXIT_FAILURE;
    }
  
  return EXIT_SUCCESS;
  
}
