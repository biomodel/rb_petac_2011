/*
 * This program joins two images (image a, image b) into a single image. 
 * This maintains the resolution and geometry of image a and overwrites it with image b 
 * only in the region where the bounds of image a and image b intersect.
 * 
 * The imaging pipeline is as follows:
 * 1. Resample image b so it matches image a
 * 2. Create a mask identifying all points in image a intersecting with image b
 * 3. Mask points in image a outside of the intersection
 * 4. Mask points in image b inside the intersection
 * 5. Add the two masked images together
 * 
 * This was written by Robbie Barnett rbar9508@uni.sydney.edu.au
 */

#include "petac.h"


int main( int argc, char* argv[] )
{
	
	if( argc < 4 )
    {
		std::cerr << "Usage: " << std::endl;
		std::cerr << argv[0] << " <image a> <image b> <image out>"  << std::endl;
		return EXIT_FAILURE;
    }

	try {
		ReaderType::Pointer readerA = ReaderType::New();
		readerA->SetFileName( argv[1] );
		try
		{
			readerA->Update();
		}
		catch (itk::ExceptionObject &ex)
		{
			std::cout << "Error in image A " << ex << std::endl;
			return EXIT_FAILURE;
		}

		ReaderType::Pointer readerB = ReaderType::New();
		readerB->SetFileName( argv[2] );
		try
		{
			readerB->Update();
		}
		catch (itk::ExceptionObject &ex)
		{
			std::cout << "Error in image B " << ex << std::endl;
			return EXIT_FAILURE;
		}


		typedef itk::ImageFileWriter< ImageType > WriterType;
		WriterType::Pointer writer = WriterType::New();
		
		writer->SetFileName( argv[3]);
		
		ImageType::ConstPointer inputA = readerA->GetOutput();
		ImageType::ConstPointer inputB = readerB->GetOutput();

		ImageType::RegionType regionA = readerA->GetOutput()->GetLargestPossibleRegion();
		ImageType::RegionType regionB = readerB->GetOutput()->GetLargestPossibleRegion();

		//  Resample image b so it matches image a
       		typedef itk::ResampleImageFilter<
                ImageType, ImageType >  ResampleFilterType;
                ResampleFilterType::Pointer resamplerB = ResampleFilterType::New();
		resamplerB->SetInput(inputB);
		resamplerB->SetSize(regionA.GetSize());
		resamplerB->SetOutputSpacing(inputA->GetSpacing());
		resamplerB->SetOutputOrigin(inputA->GetOrigin());
		resamplerB->SetDefaultPixelValue(0.0);

		// Create a mask identifying all points in image a intersecting with image b
		ImageType::Pointer maskB = ImageType::New();
		maskB -> SetRegions(regionB);
		maskB -> SetOrigin(inputB->GetOrigin());
		maskB -> SetSpacing(inputB->GetSpacing());
		maskB -> Allocate();
		maskB -> FillBuffer(1.0);
                ResampleFilterType::Pointer resamplerMaskB = ResampleFilterType::New();
		resamplerMaskB->SetInput(maskB);
		resamplerMaskB->SetSize(regionA.GetSize());
		resamplerMaskB->SetOutputSpacing(inputA->GetSpacing());
		resamplerMaskB->SetOutputOrigin(inputA->GetOrigin());
		resamplerMaskB->SetDefaultPixelValue(0.0);
		resamplerMaskB -> Update();

		// Mask points in image a outside of the intersection
		MaskNegatedFilterType::Pointer maskFilterA = MaskNegatedFilterType::New();
		maskFilterA -> SetMaskImage(resamplerMaskB->GetOutput());
		maskFilterA -> SetInput(inputA);
		maskFilterA -> SetOutsideValue(0);

		// Mask points in image b inside the intersection
		MaskFilterType::Pointer maskFilterB = MaskFilterType::New();
		maskFilterB -> SetMaskImage(resamplerMaskB->GetOutput());
		maskFilterB -> SetInput(resamplerB->GetOutput());
		maskFilterB -> SetOutsideValue(0);

		// Add the two masked images together
		AddFilterType::Pointer addFilter = AddFilterType::New(); 
		addFilter->SetInput1( maskFilterA -> GetOutput() );
		addFilter->SetInput2( maskFilterB -> GetOutput());

		writer -> SetInput(addFilter -> GetOutput());

		try
		{
			writer->Update();
		}
		catch (itk::ExceptionObject &ex)
		{
			std::cout << ex << std::endl;
			return EXIT_FAILURE;
		}

		
	
	
	}
		catch (itk::ExceptionObject &ex)
    {
		std::cout << ex << std::endl;
		return EXIT_FAILURE;
    }
	
	return EXIT_SUCCESS;
	
}
