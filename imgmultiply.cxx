/*
 * This program mutlitplies two images (image a, image b) into a single image. 
 * This maintains the resolution and geometry of image a and attempts to add image b 
 * 
 * The imaging pipeline is as follows:
 * 1. Multiply the two images together
 * 
 * This was written by Robbie Barnett rbar9508@uni.sydney.edu.au
 */

#include "petac.h"


int main( int argc, char* argv[] )
{
	
	if( argc < 4 )
    {
		std::cerr << "Usage: " << std::endl;
		std::cerr << argv[0] << " <image a> <image b> <image out>"  << std::endl;
		return EXIT_FAILURE;
    }

	try {
		ReaderType::Pointer readerA = ReaderType::New();
		readerA->SetFileName( argv[1] );
		try
		{
			readerA->Update();
		}
		catch (itk::ExceptionObject &ex)
		{
			std::cout << "Error in image A " << ex << std::endl;
			return EXIT_FAILURE;
		}

		ReaderType::Pointer readerB = ReaderType::New();
		readerB->SetFileName( argv[2] );
		try
		{
			readerB->Update();
		}
		catch (itk::ExceptionObject &ex)
		{
			std::cout << "Error in image B " << ex << std::endl;
			return EXIT_FAILURE;
		}


		typedef itk::ImageFileWriter< ImageType > WriterType;
		WriterType::Pointer writer = WriterType::New();
		
		writer->SetFileName( argv[3]);
		
		ImageType::ConstPointer inputA = readerA->GetOutput();
		ImageType::ConstPointer inputB = readerB->GetOutput();

		ImageType::RegionType regionA = readerA->GetOutput()->GetLargestPossibleRegion();
		ImageType::RegionType regionB = readerB->GetOutput()->GetLargestPossibleRegion();

		// Add the two images together
		MultiplyFilterType::Pointer multiplyFilter = MultiplyFilterType::New(); 
		multiplyFilter->SetInput1( inputA );
		multiplyFilter->SetInput2( inputB );

		writer -> SetInput(multiplyFilter -> GetOutput());

		try
		{
			writer->Update();
		}
		catch (itk::ExceptionObject &ex)
		{
			std::cout << ex << std::endl;
			return EXIT_FAILURE;
		}

		
	
	
	}
		catch (itk::ExceptionObject &ex)
    {
		std::cout << ex << std::endl;
		return EXIT_FAILURE;
    }
	
	return EXIT_SUCCESS;
	
}
